﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

namespace PlayerShip
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private float playerShipSpeed = 10;
        private Vector2 movementInput = Vector2.zero;
        
        private void Update()
        {
            Move();
        }

        private void Move()
        {
            var inputDirection = movementInput.normalized;
            var inPutVelocity = inputDirection * playerShipSpeed;

            var newXPos = transform.position.x + inPutVelocity.x * Time.deltaTime;
            var newYPos = transform.position.y + inPutVelocity.y * Time.deltaTime;

            transform.position = new Vector2(newXPos, newYPos);
        }

        public void OnMove(InputAction.CallbackContext context)
        {
            movementInput = context.ReadValue<Vector2>();
        }
    }
}
